package com.kazale.dtoresponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppDtoResponseApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppDtoResponseApplication.class, args);
	}
}
